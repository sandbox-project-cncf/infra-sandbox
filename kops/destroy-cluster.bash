#!/bin/bash
set -o errexit
set -o nounset

function destroy-cluster () {
    kops delete cluster --name ${NAME} --yes
}
