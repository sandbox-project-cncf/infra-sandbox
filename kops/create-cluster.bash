#!/bin/bash
set -o errexit
set -o nounset

function create-cluster () {
    NAME=$1
    KOPS_STATE_STORE=$2
    KOPS_STATE_STORE_OIDC=$3
    PROFILE=$4
    REGION=$5
    ZONES=$6
    CONTROL_PLANE_SIZE=$7
    WORKER_SIZE=$8
    OUTPUT_PATH=$9

    kops create cluster \
        --name=${NAME} \
        --cloud=aws \
        --zones=${ZONES} \
        --networking cilium-etcd \
        --node-size=${WORKER_SIZE}     \
        --control-plane-size=${CONTROL_PLANE_SIZE}       \
        --discovery-store=${KOPS_STATE_STORE_OIDC}/${NAME}/discovery \
        --dry-run \
        -o yaml > ${OUTPUT_PATH}${NAME}.yaml
    echo ${OUTPUT_PATH}${NAME}.yaml

    kops create -f ${OUTPUT_PATH}${NAME}.yaml
    kops update cluster --name ${NAME} --yes --admin
}

function validate-cluster() {
    kops validate cluster --wait 15m
    kubectl -n kube-system get po
    #Networking kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.10.0/deploy/static/provider/scw/deploy.yaml
    kubectl -n default apply -f kops/validation-job.yaml
    export VALIDATION_POD_NAME="$(kubectl get pods -o json | jq -r '.items[] | select(.metadata.name | test("validation-")).metadata.name')"
    sleep 15
    kubectl logs ${VALIDATION_POD_NAME}
    sleep 15
    kubectl delete pod ${VALIDATION_POD_NAME}
}
