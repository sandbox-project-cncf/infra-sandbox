#!/bin/bash
source ./kops/create-cluster.bash

export NAME=qa.cloudcntest.com
export PROFILE=kops
export REGION=us-west-2
export ZONES=us-west-2a
export CONTROL_PLANE_SIZE=t3.medium
export WORKER_SIZE=t3.medium
export OUTPUT_PATH=kops/clusters/qa/qa-

export KOPS_STATE_STORE=s3://cloudcntest-com-state-store
export KOPS_STATE_STORE_OIDC=s3://cloudcntest-com-oidc-store

create-cluster ${NAME} ${KOPS_STATE_STORE} ${KOPS_STATE_STORE_OIDC} ${PROFILE} ${REGION} ${ZONES} ${CONTROL_PLANE_SIZE} ${WORKER_SIZE} ${OUTPUT_PATH}
validate-cluster
#Networking kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.10.0/deploy/static/provider/scw/deploy.yaml