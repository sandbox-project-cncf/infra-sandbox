#!/bin/bash
source ./kops/destroy-cluster.bash

export KOPS_STATE_STORE=s3://cloudcntest-com-state-store
export NAME=sandbox.cloudcntest.com

destroy-cluster