variable namespace_name {
  type        = string
  default     = "development"
  description = "The name of the namespace"
}
