resource "kubernetes_namespace" "this" {
  metadata {
    annotations = {
      name = var.namespace_name
    }
    name = var.namespace_name
  }
}