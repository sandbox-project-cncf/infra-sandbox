terraform {
    required_version = ">= 1.7.5"
    /*
    backend "s3" {
      bucket = "cloudcntest-com-terraform"
      key    = "sandbox"
      region = "us-east-1"
    }
    */
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.27.0"
    }
  }
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
}