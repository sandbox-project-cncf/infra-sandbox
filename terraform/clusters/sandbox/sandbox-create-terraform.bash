#!/bin/bash
cd terraform/clusters/sandbox && terraform init 
terraform plan -no-color > sandbox-plan.txt
terraform plan -out sandbox-plan
terraform apply "sandbox-plan"