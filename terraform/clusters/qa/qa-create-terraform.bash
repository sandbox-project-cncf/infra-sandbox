#!/bin/bash
cd terraform/clusters/qa && terraform init 
terraform plan -no-color > qa-plan.txt
terraform plan -out qa-plan
terraform apply "qa-plan"