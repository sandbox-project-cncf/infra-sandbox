.DEFAULT_GOAL := k8s-clusters

k8s-clusters:
	kubectx -c

## SANDBOX ##
.PHONY:sandbox-create-cluster sandbox-cluster-resources
sandbox-create-cluster:
	bash kops/clusters/sandbox/sandbox-create-cluster.bash

.PHONY:sandbox-terraform
sandbox-terraform: sandbox-create-cluster
	bash terraform/clusters/sandbox/sandbox-create-terraform.bash

sandbox-cluster-resources: sandbox-terraform
	echo 'sandbox setup'

## QA ##
.PHONY:qa-create-cluster qa-cluster-resources
qa-create-cluster:
	bash kops/clusters/qa/qa-create-cluster.bash

.PHONY:qa-terraform
qa-terraform: qa-create-cluster
	bash terraform/clusters/qa/qa-create-terraform.bash

qa-cluster-resources: qa-terraform
	echo 'qa setup'

