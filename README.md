# Sandbox Infrastructure




## Kubernetes Management

To create a cluster, run the matching Make file command for the chosen environment
``` shell
 make sandbox-cluster-resources
```

To completely destroy the cluster run the appropriate destroy bash script. 
This is not reversible and all Kuberentes components are destroyed from that cluster, including the `etcd`
``` shell
 bash kops/clusters/sandbox/sandbox-destroy-cluster.bash
```